﻿using HtmlAgilityPack;
using PIMobile.Models;
using PIMobile.Models.EditViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Fizzler.Systems.HtmlAgilityPack;
using System.Web;

namespace PIMobile
{
    public class ViewModelsFactory
    {
        public InputElementModel CreateInputElement(HtmlNode node)
        {
            var name = node.Attributes["name"].Value;
            var value = this.GetValue(node.Attributes["value"]);
            var element = this.CreateConcrete(node, name, value);
            return element;
        }

        public StudyViewModel CreateStudyViewModel(HtmlNode studyRow)
        {
            var id = studyRow.Attributes["value"].Value;
            var name = studyRow.InnerText;
            var model = new StudyViewModel(id, name);
            return model;
        }

        public SiteViewModel CreateSiteViewModel(StudyViewModel study, HtmlNode studyRow)
        {
            var id = studyRow.Attributes["value"].Value;
            var name = studyRow.InnerText;
            var model = new SiteViewModel(study, id, name);
            return model;
        }

        protected virtual string GetValue(HtmlAttribute attribute)
        {
            if (attribute == null)
            {
                return "";
            }
            return attribute.Value;
        }
        protected virtual string GetCaption(HtmlNode node)
        {
            return node.ParentNode.PreviousSibling.PreviousSibling.InnerHtml;
        }
        protected virtual InputElementModel CreateConcrete(HtmlNode node, string name, string elementValue)
        {
            var attribute = node.Attributes["type"];
            var value = attribute.Value;
            if (value == "hidden")
            {
                return new HiddenInputElementModel(name, elementValue);
            }
            else if (value == "submit")
            {
                return new SubmitInputElementModel(name, elementValue);
            }
            else if (value == "password")
            {
                return new PasswordInputElementModel(this.GetCaption(node), name, elementValue);
            }
            else if (value == "text")
            {
                return new TextInputElementModel(this.GetCaption(node), name, elementValue);
            }
            else if (value == "button")
            {
                return new ButtonInputElementModel(name, elementValue);
            }
            else
            {
                return new InputElementModel(name, elementValue);
            }
        }
    }
}