﻿using HtmlAgilityPack;
using PIMobile.Models.EditViewModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PIMobile
{
    public class LoginController : Controller
    {
        public ViewModelsFactory ElementFactory { get; set; }
        public String BaseUrl { get; set; }


        public LoginController()
        {
            this.BaseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            this.ElementFactory = new ViewModelsFactory();
        }

        public ActionResult Index()
        {
            var request = new PILegacyGetRequest(this.BaseUrl);
            var value = "ZG9uZQ";
            request.AddCookie("ctmslogin", value);
            var response = request.GetUnauthorizedString();
            var loginGetDocument = new HtmlDocument();
            loginGetDocument.LoadHtml(response);
            var rootNode = loginGetDocument.DocumentNode;
            var forms = rootNode.Descendants("form");
            var loginForm = forms.First();
            var loginInputes = loginForm.Descendants("input")
                .Select(x => this.ElementFactory.CreateInputElement(x)).ToList();
            return View(new InputElements("Login", loginInputes));
        }

        [HttpPost]
        public async Task<ActionResult> Login()
        {
            var value = "ZG9uZQ";
            var cookieValue = "";
            var cookie = new Cookie("ctmslogin", value);
            var baseAddress = new Uri("http://" + this.BaseUrl);
            var cookieContainer = new CookieContainer();
            cookieContainer.Add(baseAddress, cookie);
            using (var handler = new HttpClientHandler() { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
                {
                    var values = new List<KeyValuePair<string, string>>();
                    var parameters = this.HttpContext.Request.Params;
                    foreach (var key in parameters.AllKeys)
                    {
                        values.AddRange(parameters.GetValues(key).Select(x => (new KeyValuePair<string, string>(key, x))));
                    }

                    var content = new FormUrlEncodedContent(values);
                    var response = await client.PostAsync("/services", content);
                    var responseString = await response.Content.ReadAsStringAsync();
                    var headers = response.Headers.ToDictionary(x => x.Key, x => x.Value);
                    var cookieHeader = headers["Set-Cookie"];
                    var loginHeader = cookieHeader.Single(x => x.StartsWith("ctmslogin="));
                    cookieValue = loginHeader.Split('=')[1];
                }
            }
            this.SetCookie(cookieValue);
            return RedirectToAction("Index", "Study", new RouteValueDictionary());
        }

        protected virtual void SetCookie(string cookieValue)
        {
            var cookie = new HttpCookie("ctmslogin");
            cookie.Value = cookieValue;
            this.HttpContext.Response.Cookies.Add(cookie);
        }
    }
}