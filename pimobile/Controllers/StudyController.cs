﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fizzler.Systems.HtmlAgilityPack;
using System.Text.RegularExpressions;
using PIMobile.Models;

namespace PIMobile.Controllers
{
    public class StudyController : Controller
    {
        public String BaseUrl { get; set; }
        public ViewModelsFactory ViewModelFactory { get; set; }

        public StudyController()
        {
            this.ViewModelFactory = new ViewModelsFactory();
            this.BaseUrl = ConfigurationManager.AppSettings["BaseUrl"];
        }

        // GET: Study
        public ActionResult Index()
        {
            var loginHash = this.ControllerContext.HttpContext.Request.Cookies["ctmslogin"].Value;
            var request = new PILegacyGetRequest(this.BaseUrl);
            var value = loginHash;
            request.AddCookie("ctmslogin", value);
            request.AddQueryParam("pluglet", "sites");
            var response = request.GetAuthorizedString();
            var sitesDocument = new HtmlDocument();
            sitesDocument.LoadHtml(response);
            var studies = sitesDocument.DocumentNode.QuerySelector("#drugtrialIdSrch").QuerySelectorAll("option").Skip(1);
            var studyViewModels = studies.Select(x => this.ViewModelFactory.CreateStudyViewModel(x)).ToList();
            return View(studyViewModels);
        }

        public ActionResult Sites(StudyViewModel model)
        {
            var loginHash = this.ControllerContext.HttpContext.Request.Cookies["ctmslogin"].Value;
            var request = new PILegacyGetRequest(this.BaseUrl);
            var value = loginHash;
            request.AddCookie("ctmslogin", value);
            request.AddQueryParam("pluglet", "sites");
            request.AddQueryParam("drugtrialIdSrch", model.Id);
            request.AddQueryParam("drugtrialName",model.Name);
            var response = request.GetAuthorizedString();
            var sitesDocument = new HtmlDocument();
            sitesDocument.LoadHtml(response);
            var sites = sitesDocument.DocumentNode.QuerySelector("#siteIdSrch").QuerySelectorAll("option").Skip(1);
            if (sites.Count() == 1)
            {
                return RedirectToAction("Options", this.ViewModelFactory.CreateSiteViewModel(model, sites.First()));
            }
            var sitesViewModels = sites.Select(x => this.ViewModelFactory.CreateSiteViewModel(model, x)).ToList();
            return View(sitesViewModels);
        }

        public ActionResult Options(SiteViewModel model)
        {
            if (model == null)
            {
                return RedirectToAction("Index", "Login");
            }
            return View(model);
        }
    }
}