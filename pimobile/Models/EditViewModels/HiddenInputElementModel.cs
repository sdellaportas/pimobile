﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PIMobile.Models.EditViewModels
{
    public class HiddenInputElementModel : InputElementModel
    {
        public HiddenInputElementModel(string name, string value):base(name,value) { }
    }
}
