﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PIMobile.Models.EditViewModels
{
    public class SubmitInputElementModel : InputElementModel
    {
        public SubmitInputElementModel(string name, string value):base(name,value) { }
    }
}
